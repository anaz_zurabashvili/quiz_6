package com.example.quiz_6.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_6.R
import com.example.quiz_6.abstracts.BaseFragment
import com.example.quiz_6.adapters.StatementAdapter
import com.example.quiz_6.databinding.FragmentStatementsBinding
import com.example.quiz_6.enums.Status
import com.example.quiz_6.extensions.gone
import com.example.quiz_6.extensions.visible
import com.example.quiz_6.view_models.StatementViewModel

const val TAG = "StatementFragment"
typealias Strings = R.string

class StatementFragment :
    BaseFragment<FragmentStatementsBinding, StatementViewModel>(
        FragmentStatementsBinding::inflate,
        StatementViewModel::class.java
    ) {

    private lateinit var statementAdapter: StatementAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initRV()
        coroutineScope()
    }
    private fun coroutineScope(){
        viewModel.getStatements()
        viewModel.myResponse.observe(viewLifecycleOwner, Observer {
            if (it.size != null){
                statementAdapter.statements = it
            }else{
                Log.e(TAG, "${getString(Strings.not_fount)}")
            }
        })

        viewModel.status.observe(viewLifecycleOwner, Observer {
            when (it) {
                Status.SUCCESS.toString() -> binding.progressBar.gone()
                Status.ERROR.toString() -> binding.progressBar.visible()
                else -> binding.progressBar.visible()
            }
        })
    }

    private fun initRV() =
        binding.rvStatements.apply {
            statementAdapter = StatementAdapter()
            adapter = statementAdapter
            layoutManager = LinearLayoutManager(view?.context)
        }
}