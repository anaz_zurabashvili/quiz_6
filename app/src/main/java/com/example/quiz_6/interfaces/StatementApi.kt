package com.example.quiz_6.interfaces

import com.example.quiz_6.models.Content
import retrofit2.Response
import retrofit2.http.GET

interface StatementApi {

    @GET("/v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab")
    suspend fun getStatements(): Response<Content>

}