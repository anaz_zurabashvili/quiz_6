package com.example.quiz_6.enums

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}