package com.example.quiz_6.abstracts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.example.quiz_6.Repository
import com.example.quiz_6.view_models.MainViewModelFactory

typealias  Inflate<T> = (LayoutInflater, ViewGroup, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding, VM : ViewModel>(
    private val inflate: Inflate<VB>,
    viewModelClass: Class<VM>
) :
    Fragment() {

    private var _binding: VB? = null
    val binding
        get() = _binding!!

    open var isSharedViewModel = false

    protected val viewModel: VM by lazy {
        val repository = Repository()
        //        I know that this is the worst thing I have done
        //        using *my* viewModelFactory in abstract viewModel, I'll fix this later
        val viewModelFactory = MainViewModelFactory(repository)
        if (isSharedViewModel)
            ViewModelProvider(requireActivity(), viewModelFactory)[viewModelClass]
        else
            ViewModelProvider(this, viewModelFactory)[viewModelClass]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        return _binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
