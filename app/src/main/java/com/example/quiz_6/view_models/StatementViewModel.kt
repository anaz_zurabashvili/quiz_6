package com.example.quiz_6.view_models

import android.util.Log
import androidx.lifecycle.*
import com.example.quiz_6.Repository
import com.example.quiz_6.enums.Status
import com.example.quiz_6.fragments.TAG
import com.example.quiz_6.models.Statement
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class StatementViewModel(private val repository: Repository) : ViewModel() {

    private var _myResponse = MutableLiveData<MutableList<Statement>>()
    val myResponse: LiveData<MutableList<Statement>>
        get() = _myResponse


    private val _status = MutableLiveData<String>()
    val status: LiveData<String>
        get() = _status

    fun getStatements() {
        viewModelScope.launch {
            _status.postValue(Status.LOADING.toString())
//            /v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab
            val response = try {
                repository.getStatements()
            } catch (e: IOException) {
                _status.postValue(Status.ERROR.toString())
                Log.e(TAG, "IOException, you might not hav internet connection")
                return@launch
            } catch (e: HttpException) {
                _status.postValue(Status.ERROR.toString())
                Log.e(TAG, "HttpException, unexpected response")
                return@launch
            }
            if (response.isSuccessful) {
                _myResponse.postValue(response.body()!!.content!!.toMutableList())
                _status.postValue(Status.SUCCESS.toString())
                Log.d(TAG, "success")
            } else {
                _status.postValue(Status.ERROR.toString())
                Log.e(TAG, "Response not successful")
            }
        }
    }
}