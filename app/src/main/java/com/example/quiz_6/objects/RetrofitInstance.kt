package com.example.quiz_6.objects

import com.example.quiz_6.interfaces.StatementApi
import com.example.quiz_6.extensions.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val api: StatementApi by lazy {
        retrofit.create(StatementApi::class.java)
    }
}