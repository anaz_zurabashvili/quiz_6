package com.example.quiz_6

import com.example.quiz_6.models.Content
import com.example.quiz_6.objects.RetrofitInstance
import retrofit2.Response

class Repository {

    suspend fun getStatements():  Response<Content> {
        return RetrofitInstance.api.getStatements()
    }

}