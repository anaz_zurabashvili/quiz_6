package com.example.quiz_6.extensions

import android.view.View
import android.widget.ImageView
import com.example.quiz_6.R
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso


fun View.showSnackBar(title:String)=
    Snackbar.make(this, title, Snackbar.LENGTH_SHORT).show()

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }

fun ImageView.loadUrl(url: String?){
    if (!url.isNullOrEmpty()){
        Picasso.get().load(url).into(this)
    }else{
        Picasso.get().load(url).placeholder(R.drawable.ic_launcher_foreground).into(this)
    }
}