package com.example.quiz_6.extensions

import kotlin.math.pow

fun Int.pow(): Int  = this.toDouble().pow(2.0).toInt()
