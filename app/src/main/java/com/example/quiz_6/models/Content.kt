package com.example.quiz_6.models

data class Content(
    val content: List<Statement>?
)