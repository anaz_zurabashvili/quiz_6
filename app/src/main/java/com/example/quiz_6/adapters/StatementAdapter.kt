package com.example.quiz_6.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_6.databinding.ItemStatementBinding
import com.example.quiz_6.extensions.loadUrl
import com.example.quiz_6.models.Statement

class StatementAdapter:RecyclerView.Adapter<StatementAdapter.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Statement>() {
        override fun areItemsTheSame(oldItem: Statement, newItem: Statement): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Statement, newItem: Statement): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    var statements: MutableList<Statement>
        get() = differ.currentList
        set(value) { differ.submitList(value) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemStatementBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: StatementAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: ItemStatementBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Statement
        fun onBind() {
            model = statements[adapterPosition]
            binding.tvTitle.text = model.titleKA
            binding.tvDescription.text = model.descriptionKA
            binding.tvPublishDate.text = model.publishDate
            binding.imCover.loadUrl(model.cover)
            binding.tvPublishDate.text = model.publishDate
        }
    }

    override fun getItemCount() = statements.size
}
